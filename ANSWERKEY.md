# Answer Key

## NOTES

- Challenge mentions _original_ NGINX config should be listening on TCP443 for host header `www2.example.com`, but this is not the case
  - Refer to last note under _Start the Demo Stack_ section
 

---

#### Minimum requirements

The following is minimum additions to be configured:

* An HTTPS service for `www2.example.com` traffic over Port 443 (You can use the self-signed certificates provided). Configure NGINX Plus SSL termination on the load balancer and proxy upstream servers over HTTP, i.e. `Client --HTTPS--> NGINX (SSL termination) --HTTP--> webserver`
    * What are some TLS Best practices that should be considered here?
    > - Do you or your CIS team require end-to-end security? If so, you might require re-encryption of traffic following termination of SSL/TLS connections. If this isn't a requirement then you can enjoy all the perks of SSL/TLS offloading.
    > - You should ensure that your load balancer, upstream servers, and clients are configured to support the same/common protocols, protocol versions, and cipher suites.
    > - If connecting to upstream servers via HTTPS you should ensure proper CA bundles are present and referenced on NGINX to ensure proper validation of internally signed certificates.
    > - Ensure proper permissions are set on the certificate and, more importantly, private key -- particularly true if the cert is a wildcard
* HTTP to HTTPS redirect service for `www2.example.com`, i.e., `Client --HTTP--> NGINX (redirect) --HTTPS--> NGINX (SSL termination) --HTTP--> webserver`

```bash
server {
        server_name www2.example.com;
        listen 80 ;
        location / {
                return 301 https://$host$request_uri;
        }
}
```

* Enable [keepalive connections](https://www.nginx.com/blog/http-keepalives-and-web-performance/) to upstream servers. 
    * How would you test and confirm this has been enabled?
> - Enabled by default. Confirm via request headers in browser developer console (looking for the presence of `Connection: keep-alive` in the headers) or via curl:
```
> curl -v -k -I https://www2.example.com:8023/ https://www2.example.com:8023/`
...
* Re-using existing connection! (#0) with host www2.example.com
* Connected to www2.example.com (192.168.1.55) port 8023 (#0)
...
* Connection #0 to host www2.example.com left intact
```
* Enable an Active HTTP Health Check: Periodically check the health of upstream servers by sending a custom health‑check requests to each server and verifying the correct response. e.g. check for a `HTTP 200` response and `Content-Type: text/html`

```
## example.com.conf

match a_ok {
        status 200;
        header Content-Type = text/html;
}

# www.example.com HTTP
server {
    listen 80 default_server;
    server_name www.example.com "";
    status_zone www.example.com_http;
    location / {
        proxy_pass http://nginx_hello;
        health_check match=a_ok;
    }

```

* Enable an HTTP load balancing algorithm methods **other than the default**, round-robin  
```
# upstreams.conf -- ip_hash determined from first three octects of client IP
upstream nginx_hello {

    # Load Balancing Algorithm
    # Default - Round Robin Load Balancing

    ip_hash;

    zone nginx_hello 64k;
    server nginx1:80;
    server nginx2:80;

}
```
* Create a `HTTP 301` URL redirect for `/old-url` to `/new-url`

#### Extra Credits  

Enable any of the following features on the NGINX Plus load balancer for extra credits:

* Provide the [`curl`](https://ec.haxx.se/http-cheatsheet.html) (or similar tool) command to add and remove the server from the NGINX upstream named `dynamic` via the NGINX API. 

```bash
export API_NGINX=192.168.1.184:8021
api_data() {
cat << EOF
    {
        "server": "172.19.0.2:80",
        "weight": 4,
        "max_conns": 0,
        "max_fails": 0,
        "fail_timeout": "10s",
        "slow_start": "10s",
        "backup": true,
        "down": true
    }
EOF
}

curl -vvvv -X POST -d "$(api_data)" -s http://${API_NGINX}/api/6/http/upstreams/dynamic/servers


### To delete by ID, which may vary:
export API_DYNAMIC_ID=$(curl http://${API_NGINX}/api/6/http/upstreams/dynamic/servers/ | jq '.[0].id')
curl -X DELETE -s http://${API_NGINX}/api/6/http/upstreams/dynamic/servers/${API_DYNAMIC_ID}

```

* Enable Proxy caching for **image files only**. Use the Cache folder provisioned on `/var/cache/nginx`, i.e. set `proxy_cache_path` to `/var/cache/nginx`. Validate the test image http://www.example.com/smile.png is cached on NGINX

```
# Ensure default port
proxy_cache_path /var/cache/nginx keys_zone=CACHE:100m inactive=60m max_size=25m;

    location ~* .*\.(?:svg|svgz|jpe?g|gif|png|ico|bmp)$ {
        proxy_cache CACHE;
        expires max;
    }
```

> Ran into trouble getting cache HITs working -- cache was properly enabled/initialized but it never cached *anything*. Tested with upstream NGINX docker image and a pared-down config (e.g. removed 'match' statements, 'state' statements, pointed at a different web server):
```
docker run --name nginx-test --rm -p 80:80 -p 443:443 -v /tmp/nginx-plus/etc/nginx:/etc/nginx -v /tmp/nginx-plus/etc/ssl:/etc/ssl nginx
```

> This upstream deployment + example worked fine (observe initial MISS followed by HIT):
```
192.168.1.110 - - [18/Feb/2021:16:03:33 +0000] "GET /assets/favicon-7901bd695fb93edb07975966062049829afb56cf11511236e61bcf425070e36e.png HTTP/1.1" 200 1611 "http://www.example.com/users/sign_in" "" "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36" "-" "www.example.com" sn="www.example.com" rt=0.023 ua="192.168.1.55:80" us="200" ut="0.023" ul="1611" cs=MISS c9926c17a89bd042e9f1b42f1eab7ddf
192.168.1.110 - - [18/Feb/2021:16:03:36 +0000] "GET /users/sign_in HTTP/1.1" 304 0 "-" "" "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36" "-" "www.example.com" sn="www.example.com" rt=0.000 ua="-" us="-" ut="-" ul="-" cs=HIT 673e9ed801cd07ad3071dbd06ac9054c
192.168.1.110 - - [18/Feb/2021:16:03:36 +0000] "GET /assets/favicon-7901bd695fb93edb07975966062049829afb56cf11511236e61bcf425070e36e.png HTTP/1.1" 200 1611 "http://www.example.com/users/sign_in" "" "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36" "-" "www.example.com" sn="www.example.com" rt=0.000 ua="-" us="-" ut="-" ul="-" cs=HIT 393d3bca383139c8bee72d4731cd4fa9

```

> Reviewed upstream server config and needed to comment/remove `expires -1;` parameter for proper caching of image files _only_.
```
nginx-plus_1  | 192.168.1.110 - - [18/Feb/2021:21:12:23 +0000] "GET /smile.png HTTP/1.1" 304 0 "-" "" "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36" "-" "www.example.com" sn="www.example.com" rt=0.000 ua="-" us="-" ut="-" ul="-" cs=HIT 585a8963223e120f9a69629a8f905e26
nginx-plus_1  | 192.168.1.110 - - [18/Feb/2021:21:12:23 +0000] "GET / HTTP/1.1" 200 7225 "http://www.example.com/" "" "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36" "-" "www.example.com" sn="www.example.com" rt=0.001 ua="172.18.0.3:80" us="200" ut="0.001" ul="7212" cs=- 92f83457cbf4b00da3885870c3b46715
```

> Further testing:
```
curl -I http://www.example.com/smile.png -o temp.png -vvvvv ### Should show X-Cache-Status: HIT|MISS
curl -I http://www.example.com/ -vvvvv                      ### Should not show X-Cache-Status
```

> It was helpful to debug the regex used (particularly for catching all images at all paths) using a couple methods:
> `git clone https://github.com/nginxinc/NGINX-Demos.git; cd NGINX-Demos/nginx-regex-tester/; docker-compose up`
> or via access on third-party regex tester (e.g. https://nginx.viraptor.info/ )

* Enable Cache Purge API with Access Controls: Configure Cache Purge to enable purging content from the proxy cache via an API call and demonstrate security considerations by restricting access to the Purge Command using one of three implementations:
  *  **Enforcing an allowlist of explicit Client IP Addresses or IP Address ranges that can make Cache Purge API calls from, or**
  *  Enforcing an allowlist of Client API keys that are required on Cache Purge API calls
  *  A combination of enforcing Client IP Addresses and Client API keys 

> Configure with
```
proxy_cache_path /var/cache/nginx levels=1:2 keys_zone=default:10m inactive=60m loader_threshold=300 loader_files=200 max_size=1g use_temp_path=off purger=on;

map $request_method $purge_method {
        PURGE 1;
        default 0;
}

geo $purge_allowed {
       default         0;
       192.168.0.0/16  1;
}

map $request_method $purge_method {
       PURGE   $purge_allowed;
       default 0;
}

# www.example.com HTTP
server {
    listen 80 default_server;
    server_name www.example.com "";
    status_zone www.example.com_http;


    location ~* \.(?:svg|svgz|jpe?g|gif|png|ico|bmp)$ {
        proxy_cache default;
        proxy_ignore_headers "Cache-Control";
        proxy_ignore_headers "Set-Cookie";
        proxy_cache_valid 200 30m;
        proxy_cache_methods GET HEAD POST;

        add_header X-Cache-Status $upstream_cache_status;

        proxy_pass http://nginx_hello;

        proxy_cache_purge $purge_method;
    }

```
> Tested with
```
 curl -X PURGE -D - "http://www.example.com/smile.png"
## Reload page for above and observe MISS; reload again and observe HIT
```

* Enable any Session persistence method and demonstrate the routing all requests from a user session to the same upstream server
```
# For upstream servers:
sticky cookie srv_id expires=1h domain=.example.com path=/
```

Observe set & persistent `srv_id` in _Chrome DevTools > Application > Cookies > <SITE>_ -- Try via `curl` request with headers and observe changing `srv_id` (since each request is unique)

* Enable Weighted Round Robin load balancing and demonstrate the load distribution using a simple bash/shell script

>weight=(n) for each server in upstream definition

```bash
# Test with 70/30 split
var=$(for ((i=1;i<=10;i++)); do (curl http://www.example.com/ | grep '<span>172.*</span>' | awk '{print $2}' | sed 's/\(<span>\|<\/span><\/p>\)//g'); done)
printf "%s\n" $var | sort | uniq -c

```


* Provide the command to execute the NGINX command on the running container, e.g., `nginx -t` to check nginx config file and `nginx -s reload` to reload the configuration file.

```bash
docker exec -it nginx-se-challenge_nginx-plus_1 nginx -t
docker exec -it nginx-se-challenge_nginx-plus_1 nginx -s reload
```

* Add another web server instance in the `docker-compose.yml` file, using the same [nginx-hello](https://github.com/nginxinc/NGINX-Demos/tree/master/nginx-hello), with the hostname, `nginx3`, and add the new server to the upstream group, `nginx_hello`.


```bash
# Added 3rd with weighted round-robin + test with 70/20/10 split
var=$(for ((i=1;i<=10;i++)); do (curl http://www.example.com/ | grep '<span>172.*</span>' | awk '{print $2}' | sed 's/\(<span>\|<\/span><\/p>\)//g'); done)
printf "%s\n" $var | sort | uniq -c

```
